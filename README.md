# Backend Code Project

## Purpose
This project is meant to test an applicant's ability to design software, choose appropriate technologies and alogorithms, and communicate their decisions. The project is designed to be completed in less than four hours, although you may take more time if you would like.

## Project Description
Write a basic resume parser that will extract the job seeker's email address and phone number from plain text resumes and store the extracted information in a SQLite database table. Your program should parse all resumes in a given directory. There are sample resume inputs included in this repository, but your solution should work for a variety of plain text inputs with commonly accepted resume formatting. You may use any resources that would normally be available to you in your daily work (e.g., online documentation, forums, open source libraries, etc.). 

## Implementation Notes
- Use any language of your choice
- You may use open-source libraries of your choice, but be prepared to discuss what they are doing
- Your solution must be able to run on Linux Debian 11 Bullseye (Docker is an excellent option, but not required)
- Provide instructions on how to run/test your code, including any instructions for required packages
- If you are unsure about any of the project requirements, use your best judgement and be prepared to defend your decision

## Submission
- Submit your solution by creating a Gitlab repository (you can fork this repo) and email us the link. 
- Your repository should contain a README with a concise description of your project and instructions on how to compile and run your code.

## Code Review
After you submit your solution, we will schedule a code review interview with you. You should be prepared to:
- Give a live demonstration of your solution
- Explain what the code is doing
- Defend your design decisions 
- Answer questions about libraries you chose to use
- Discuss alternate solutions. What other solutions did you consider? Why did you choose your approach? 