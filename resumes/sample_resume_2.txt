Janet Smith
1030 Aponi Pl.
Moscow, ID 83843
210-392-8234
janetsmith834201@gmail.com

Accomplishments:
- Earned the Zach Bachmann award for graphic design excellence two years in a row
- Led an award winning marketing innovation team at the AGF conference


Summary:
Graphic Designer with over 5 years of experience in creating visually appealing designs for various mediums such as print, web, and digital. Skilled in using industry-standard software and tools, including Adobe Creative Suite (Photoshop, Illustrator, InDesign), Sketch, and Figma. Strong portfolio demonstrating a range of design skills, including branding, layout design, typography, and illustration. Proven ability to work in a fast-paced environment and meet tight deadlines.

Experience:

	Graphic Designer
	Amazing Company
	January 2020 - Present

	Design and create visual assets for various marketing materials such as brochures, flyers, presentations, and social media posts.
	Collaborate with the marketing team to create visually appealing designs that effectively communicate brand messaging and product features.
	Work with cross-functional teams to ensure designs are consistent with brand guidelines and meet the needs of different departments.
	Stay up-to-date with the latest design trends and technologies to ensure designs are fresh and innovative.
	Graphic Designer

	Great Ideas Company
	July 2016 - December 2019

	Worked on a wide range of projects, including branding, packaging design, and web design.
	Collaborated with the product team to create visually appealing designs that effectively communicate product features and benefits.
	Managed multiple projects simultaneously, ensuring all deliverables were completed on time and to a high standard.
	Provided guidance and mentorship to junior designers, helping them to develop their skills and improve their work.

Education:
	Bachelor of Fine Arts in Graphic Design
	University of XYZ
	2012 - 2016

Skills:
	Graphic design
	Branding
	Layout design
	Typography
	Illustration
	Adobe Creative Suite
	Sketch
	Figma
	Time management
	Communication

Portfolio: Available upon request.

References: Available upon request.
